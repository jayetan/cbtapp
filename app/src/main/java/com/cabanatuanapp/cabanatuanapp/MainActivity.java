package com.cabanatuanapp.cabanatuanapp;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cabanatuanapp.fragments.HistoryContent;
import com.cabanatuanapp.fragments.LandmarksFragment;
import com.cabanatuanapp.fragments.MainFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RequestQueue requestQueue;

    public  String getDateCurrentTimeZone(long timestamp) {
        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getTimeZone("GMT");
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        }catch (Exception e) {
        }
        return "";
    }
    public void getWeather(){
        requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "http://api.openweathermap.org/data/2.5/weather?q=Cabanatuan,us&mode=json&appid=ce02a5be2ec65bb46925cb9de61a9ff5",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray jsonArray = response.getJSONArray("weather");

                            /*GET Temperature*/
                            JSONObject mainObj = response.getJSONObject("main");
                            double temp = mainObj.getDouble("temp");
                            double kev = 273.15;
                            double celsius = temp - kev;
                            String cel = String.valueOf(celsius + " \u2103");
                            TextView textMain = (TextView) findViewById(R.id.textView6);
                            textMain.append(cel);

                            /*GET Sunrise, Sunset*/
                            JSONObject sysObj = response.getJSONObject("sys");
                            long sunrise = sysObj.getLong("sunrise");
                            long sunset = sysObj.getLong("sunset");
                            String sunriseConv = getDateCurrentTimeZone(sunrise);
                            String sunsetConv = getDateCurrentTimeZone(sunset);
                            TextView textView7 = (TextView) findViewById(R.id.textView7);
                            TextView textView8 = (TextView) findViewById(R.id.textView8);
                            textView7.append(sunriseConv+"am");
                            textView8.append(sunsetConv+"pm");


                            for (int i=0; i<jsonArray.length(); i++){
                                JSONObject weather = jsonArray.getJSONObject(i);
                                String id = weather.getString("id");
                                String main = weather.getString("main");
                                String description = weather.getString("description");
                                String icon = weather.getString("icon");
                                String output = description.substring(0, 1).toUpperCase() + description.substring(1);


                                TextView textWeather = (TextView) findViewById(R.id.textView4);
                                textWeather.append(output);

                                RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.fragment_main);
                                ImageView imageView = (ImageView) findViewById(R.id.imageView3);


                                if (icon.equals("01d") || icon.equals("01n")){
                                    relativeLayout.setBackgroundResource(R.drawable.bg_01d);

                                    if (icon.equals("01d")){
                                        imageView.setBackgroundResource(R.drawable.ic_01d);
                                    }else{
                                        imageView.setBackgroundResource(R.drawable.ic_01n);
                                    }
                                }
                                if (icon.equals("02d") || icon.equals("02n")){
                                    relativeLayout.setBackgroundResource(R.drawable.bg_02d);

                                    if (icon.equals("02d")){
                                        imageView.setBackgroundResource(R.drawable.ic_02d);
                                    }else{
                                        imageView.setBackgroundResource(R.drawable.ic_02n);
                                    }
                                }
                                if (icon.equals("03d") || icon.equals("03n")){
                                    relativeLayout.setBackgroundResource(R.drawable.bg_03d);

                                    if (icon.equals("03d")){
                                        imageView.setBackgroundResource(R.drawable.ic_03d);
                                    }else{
                                        imageView.setBackgroundResource(R.drawable.ic_03n);
                                    }
                                }
                                if (icon.equals("04d") || icon.equals("04n")){
                                    relativeLayout.setBackgroundResource(R.drawable.bg_04d);
                                    if (icon.equals("04d")){
                                        imageView.setBackgroundResource(R.drawable.ic_04d);
                                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    }else{
                                        imageView.setBackgroundResource(R.drawable.ic_04n);
                                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    }
                                }
                                if (icon.equals("09d") || icon.equals("09n")){
                                    relativeLayout.setBackgroundResource(R.drawable.bg_09d);

                                    if (icon.equals("09d")){
                                        imageView.setBackgroundResource(R.drawable.ic_09d);
                                    }else{
                                        imageView.setBackgroundResource(R.drawable.ic_09n);
                                    }
                                }
                                if (icon.equals("10d") || icon.equals("10n")){
                                    relativeLayout.setBackgroundResource(R.drawable.bg_10d);

                                    if (icon.equals("10d")){
                                        imageView.setBackgroundResource(R.drawable.ic_10d);
                                    }else{
                                        imageView.setBackgroundResource(R.drawable.ic_10n);
                                    }
                                }
                                if (icon.equals("11d") || icon.equals("11n")){
                                    relativeLayout.setBackgroundResource(R.drawable.bg_11d);

                                    if (icon.equals("11d")){
                                        imageView.setBackgroundResource(R.drawable.ic_11d);
                                    }else{
                                        imageView.setBackgroundResource(R.drawable.ic_11n);
                                    }
                                }
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley", "weather error");
                    }
                }


        );

        requestQueue.add(jsonObjectRequest);
        getUpdate();
    }

    public void getUpdate(){
        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "http://cabanatuanapp.esy.es/api/",

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            /*GET Update*/
                            JSONObject update = response.getJSONObject("update");
                            String date = update.getString("date");
                            String time = update.getString("time");
                            String title = update.getString("title");
                            String content = update.getString("content");

                            TextView textUpdate = (TextView) findViewById(R.id.textUpdate);
                            textUpdate.append(
                                    date + " " + time + "\n" + title + "\n" + content
                            );


                        } catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle your error types accordingly.For Timeout & No connection error, you can show 'retry' button.
                        // For AuthFailure, you can re login with user credentials.
                        // For ClientError, 400 & 401, Errors happening on client side when sending api request.
                        // In this case you can check how client is forming the api and debug accordingly.
                        // For ServerError 5xx, you can do retry or handle accordingly.
                        if( error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                        } else if( error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                        } else if( error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                        } else if( error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                        } else if( error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                        } else if( error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError");
                        }

                    }
                }


        );

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();

        getWeather();
        Intent intent = new Intent(this, UpdateService.class);
        startService(intent);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        FragmentManager fm = getFragmentManager();

        int id = item.getItemId();

        if (id == R.id.nav_weather_updates) {

            fm.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
            setTitle("Weather and Updates");
            getWeather();

        } else if (id == R.id.nav_landmarks) {

            fm.beginTransaction().replace(R.id.content_frame, new LandmarksFragment()).commit();
            setTitle("Landmarks");

        } else if (id == R.id.nav_history) {

            fm.beginTransaction().replace(R.id.content_frame, new HistoryContent()).commit();
            setTitle("History");

        }/* else if (id == R.id.nav_manage) {
            Intent myIntent = new Intent(MainActivity.this, MapsActivity.class);
            Bundle b = new Bundle();
            b.putDouble("lat", 15.4865824);
            b.putDouble("lng", 120.9731943);
            myIntent.putExtras(b);
            MainActivity.this.startActivity(myIntent);

        }*//* else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
