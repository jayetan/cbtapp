package com.cabanatuanapp.cabanatuanapp;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Bundle b = getIntent().getExtras();
        Double lat = b.getDouble("lat");
        Double lng = b.getDouble("lng");

        LatLng dynamicPos = new LatLng(lat, lng);
        LatLng cabanatuan = new LatLng(15.4865047, 120.973393);
        LatLng sm = new LatLng(15.4656304, 120.9553638);
        LatLng genLuna = new LatLng(15.4894745, 120.9638799);
        LatLng campPangatian = new LatLng(15.5101101, 121.0446185);
        LatLng oldCapitol = new LatLng(15.4881334, 120.96968651);
        LatLng neMall = new LatLng(15.460484, 120.948575);
        LatLng robinsons = new LatLng(15.460001, 120.950357);
        LatLng megaCenter = new LatLng(15.487678, 120.967884);

        mMap.addMarker(new MarkerOptions().position(cabanatuan).title("Cabanatuan City"));
        mMap.addMarker(new MarkerOptions().position(sm).title("SM Cabanatuan"));
        mMap.addMarker(new MarkerOptions().position(genLuna).title("Gen. Luna Statue"));
        mMap.addMarker(new MarkerOptions().position(campPangatian).title("Camp Pangatian"));
        mMap.addMarker(new MarkerOptions().position(oldCapitol).title("Old Capitol"));
        mMap.addMarker(new MarkerOptions().position(neMall).title("NE Pacific Mall"));
        mMap.addMarker(new MarkerOptions().position(robinsons).title("Robinsons Mall"));
        mMap.addMarker(new MarkerOptions().position(megaCenter).title("SM Mega Center"));


        if(lat != null && lng != null){
            mMap.moveCamera(CameraUpdateFactory.newLatLng(dynamicPos));
            mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 15.0f) );
        }else{
            mMap.moveCamera(CameraUpdateFactory.newLatLng(cabanatuan));
            mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(15.4865047, 120.973393), 7.0f) );
        }

    }
}
