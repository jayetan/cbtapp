package com.cabanatuanapp.cabanatuanapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class UpdateService extends Service {

    private final String TAG = "customTag";
    private static Timer timer = new Timer();
    private int num = 0;
    RequestQueue requestQueue;

    public UpdateService() {
    }
    public void getUpdate(){
        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "http://cabanatuanapp.esy.es/api/",

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            /*GET Update*/
                            JSONObject update = response.getJSONObject("update");
                            String id = update.getString("id");
                            int idInt = Integer.parseInt(id);
                            String date = update.getString("date");
                            String time = update.getString("time");
                            String title = update.getString("title");
                            String content = update.getString("content");
                            if(idInt > num){
                                final Intent intent = new Intent(UpdateService.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                        Intent.FLAG_ACTIVITY_NEW_TASK);
                                PendingIntent pendingIntent = PendingIntent.getActivity(UpdateService.this, 0, intent, 0);
                                Notification notification = new Notification.Builder(UpdateService.this)
                                        .setTicker("New Update")
                                        .setContentTitle(title)
                                        .setContentText(content)
                                        .setSmallIcon(R.mipmap.ic_launcher)
                                        .setContentIntent(pendingIntent).build();
                                notification.flags = Notification.FLAG_AUTO_CANCEL;
                                NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                                notificationManager.notify(0, notification);
                                num = idInt;
                            }


                        } catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle your error types accordingly.For Timeout & No connection error, you can show 'retry' button.
                        // For AuthFailure, you can re login with user credentials.
                        // For ClientError, 400 & 401, Errors happening on client side when sending api request.
                        // In this case you can check how client is forming the api and debug accordingly.
                        // For ServerError 5xx, you can do retry or handle accordingly.
                        if( error instanceof NetworkError) {
                            Log.e("Volley", "NetworkError");
                        } else if( error instanceof ServerError) {
                            Log.e("Volley", "ServerError");
                        } else if( error instanceof AuthFailureError) {
                            Log.e("Volley", "AuthFailureError");
                        } else if( error instanceof ParseError) {
                            Log.e("Volley", "ParseError");
                        } else if( error instanceof NoConnectionError) {
                            Log.e("Volley", "NoConnectionError");
                        } else if( error instanceof TimeoutError) {
                            Log.e("Volley", "TimeoutError");
                        }

                    }
                }


        );

        requestQueue.add(jsonObjectRequest);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Runnable r = new Runnable() {
            @Override
            public void run() {

                timer.schedule( new TimerTask() {
                    public void run() {
                        getUpdate();
                    }
                }, 0, 60*30000);
            }
        };
        Thread myThread = new Thread(r);
        myThread.start();
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }
}
