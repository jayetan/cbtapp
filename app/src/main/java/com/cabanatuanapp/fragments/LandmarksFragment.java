package com.cabanatuanapp.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.cabanatuanapp.cabanatuanapp.MapsActivity;
import com.cabanatuanapp.cabanatuanapp.R;

/**
 * Created by jei on 11/2/15.
 */
public class LandmarksFragment extends Fragment {
    ImageButton floatButton1, floatButton2, floatButton3, floatButton4, floatButton5, floatButton6, floatButton7;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_landmarks, container, false);
        floatButton1 = (ImageButton)rootView.findViewById(R.id.floatBtn1);
        floatButton2 = (ImageButton)rootView.findViewById(R.id.floatBtn2);
        floatButton3 = (ImageButton)rootView.findViewById(R.id.floatBtn3);
        floatButton4 = (ImageButton)rootView.findViewById(R.id.floatBtn4);
        floatButton5 = (ImageButton)rootView.findViewById(R.id.floatBtn5);
        floatButton6 = (ImageButton)rootView.findViewById(R.id.floatBtn6);
        floatButton7 = (ImageButton)rootView.findViewById(R.id.floatBtn7);

        floatButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), MapsActivity.class);
                Bundle b = new Bundle();
                b.putDouble("lat", 15.4656304);
                b.putDouble("lng", 120.9553638);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });

        floatButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), MapsActivity.class);
                Bundle b = new Bundle();
                b.putDouble("lat", 15.4894745);
                b.putDouble("lng", 120.9638799);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });

        floatButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), MapsActivity.class);
                Bundle b = new Bundle();
                b.putDouble("lat", 15.5101101);
                b.putDouble("lng", 121.0446185);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });
        floatButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), MapsActivity.class);
                Bundle b = new Bundle();
                b.putDouble("lat", 15.4881334);
                b.putDouble("lng", 120.96968651);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });
        floatButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), MapsActivity.class);
                Bundle b = new Bundle();
                b.putDouble("lat", 15.460484);
                b.putDouble("lng", 120.948575);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });
        floatButton6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), MapsActivity.class);
                Bundle b = new Bundle();
                b.putDouble("lat", 15.460001);
                b.putDouble("lng", 120.950357);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });
        floatButton7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), MapsActivity.class);
                Bundle b = new Bundle();
                b.putDouble("lat", 15.487678);
                b.putDouble("lng", 120.967884);
                myIntent.putExtras(b);
                startActivity(myIntent);
            }
        });
        return rootView;
    }
}
