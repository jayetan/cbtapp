package com.cabanatuanapp.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cabanatuanapp.cabanatuanapp.R;

/**
 * Created by jei on 11/2/15.
 */
public class HistoryContent extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.content_history, container, false);
        TextView textView = (TextView)rootView.findViewById(R.id.contentAdditional);

        int resId = getActivity().getResources().getIdentifier("contentAdditional", "string",
                "com.cabanatuanapp.cabanatuanapp");

        Spanned spanned = Html.fromHtml(this.getString(resId));
        textView.setText(spanned);
        return rootView;
    }
}
